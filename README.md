# plus_slurm
A python module to make it simple to submit jobs to the Slurm based cluster directly from python

## Documentation
https://plus-slurm.readthedocs.io

## Code
The source code can be found here: <https://gitlab.com/thht/plus-slurm>

## License
This module is developed by Thomas Hartmann at the Universität Salzburg. You are free to use, copy, modify, distribute it under the terms of the GPL3.
