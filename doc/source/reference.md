# Reference

```{eval-rst}
.. automodule:: plus_slurm

   .. autoclass:: JobCluster
      :members:
      
   .. autoclass:: ApptainerJobCluster
      :show-inheritance:
      :members:

   .. autoclass:: Job
      :members:

   .. autoclass:: AutomaticFilenameJob
      :show-inheritance:
      :members:

   .. autoclass:: PermuteArgument
```
